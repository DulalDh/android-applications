package com.edu.bd.sastcollege;


import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;


public class GalleryActivity extends AppCompatActivity {

    private RecyclerView mGalleryList;
    private DatabaseReference mDatabase;

    //progress dialog
 //   private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("GalleryUploads");
        mGalleryList = (RecyclerView)findViewById(R.id.recyclerView);
        mGalleryList.setHasFixedSize(true);
        mGalleryList.setLayoutManager(new LinearLayoutManager(this));

    /*    //displaying progress dialog while fetching images
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
*/

        FirebaseRecyclerAdapter<UpdateGallery, GalleryViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<UpdateGallery, GalleryViewHolder>(
                UpdateGallery.class,
                R.layout.gallery_images,
                GalleryViewHolder.class,
                mDatabase
        ) {
            @Override
            protected void populateViewHolder(GalleryViewHolder viewHolder, UpdateGallery model, int position) {
                viewHolder.setTitle(model.getTitle());
                viewHolder.setDesc(model.getDesc());
                viewHolder.setImage(getApplicationContext(), model.getImage());
                viewHolder.setTime(model.getTime());
           //     progressDialog.dismiss();
            }
        };
        mGalleryList.setAdapter(firebaseRecyclerAdapter);

    }

    /*@Override
    protected void onStart() {
        super.onStart();


    }*/

    private static class GalleryViewHolder extends RecyclerView.ViewHolder {
        View mView;
        public GalleryViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setTitle(String title){
            TextView postTitle = (TextView) mView.findViewById(R.id.post_title);
            postTitle.setText(title);
        }


        private void setImage(Context ctx, String image){
            ImageView galleryImage = (ImageView) mView.findViewById(R.id.post_image);
            Picasso.with(ctx).load(image).into(galleryImage);
        }

        private void setDesc(String desc){
            TextView postDesc = (TextView) mView.findViewById(R.id.post_dec);
            postDesc.setText(desc);
        }

        public void setTime(String time){
            TextView postDesc = (TextView) mView.findViewById(R.id.post_time);
            postDesc.setText(time);
        }
    }
}

/*    //recyclerview object
    private RecyclerView recyclerView;

    //adapter object
    private RecyclerView.Adapter adapter;

    //database reference
    private DatabaseReference mDatabase;

    //progress dialog
    private ProgressDialog progressDialog;

    //list to hold all the uploaded images
    private List<UpdateGallery> uploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        progressDialog = new ProgressDialog(this);

        uploads = new ArrayList<>();
        //displaying progress dialog while fetching images
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.GALLERY_DATABASE_PATH_UPLOADS);
        onStart();

    }

    @Override
    protected void onStart() {
        super.onStart();

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                progressDialog.dismiss();

                //iterating through all the values in database
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    UpdateGallery upload = postSnapshot.getValue(UpdateGallery.class);
                    uploads.add(upload);
                }
                //creating adapter
                adapter = new GalleryAdapter(getApplicationContext(), uploads);

                //adding adapter to recyclerview
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }*/
