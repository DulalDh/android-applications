package com.edu.bd.sastcollege;

/**
 * Created by DULAL on 8/11/2018.
 */

public class Cse {

    private String name;
    private int id_name;

    public Cse(String name,int id_name) {
        this.setName(name);
        this.setId_name(id_name);
    }


    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }




    public int getId_name(){
        return id_name;
    }
    public void setId_name(int id_name){
        this.id_name=id_name;
    }


}
