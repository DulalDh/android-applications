package com.edu.bd.sastcollege;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EmployeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employe);

    }

    public void call1(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01717318578"));
        startActivity(intent);
    }

    public void call2(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01723044909"));
        startActivity(intent);
    }

    public void call3(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01735329669"));
        startActivity(intent);
    }

    public void call4(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01702321060"));
        startActivity(intent);
    }

    public void call5(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01740141604"));
        startActivity(intent);
    }

    public void call6(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01740212932"));
        startActivity(intent);
    }

    public void call7(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01751006817"));
        startActivity(intent);
    }

    public void call8(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01774232470"));
        startActivity(intent);
    }

    public void call9(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01738268881"));
        startActivity(intent);
    }
}
