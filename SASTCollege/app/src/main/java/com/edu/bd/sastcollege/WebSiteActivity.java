package com.edu.bd.sastcollege;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebSiteActivity extends AppCompatActivity {

    private WebView myWebview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_site);

        myWebview = (WebView)findViewById(R.id.website);
        WebSettings webSettings = myWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebview.loadUrl("http://sastcollege.edu.bd/");
        myWebview.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if(myWebview.canGoBack())
            myWebview.goBack();
        else
            super.onBackPressed();
    }
}
