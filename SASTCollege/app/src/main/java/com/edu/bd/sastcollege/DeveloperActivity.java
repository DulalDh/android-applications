package com.edu.bd.sastcollege;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DeveloperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);

    }

    public void call1(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01763235521"));
        startActivity(intent);
    }

    public void call2(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01751341563"));
        startActivity(intent);
    }

    public void call3(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01767318962"));
        startActivity(intent);
    }
}
