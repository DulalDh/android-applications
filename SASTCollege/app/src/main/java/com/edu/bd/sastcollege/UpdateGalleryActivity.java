package com.edu.bd.sastcollege;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.DateFormat;
import java.util.Calendar;

public class UpdateGalleryActivity extends AppCompatActivity {

    private static final int GALLERY_REQUEST = 1;
    private ImageButton mSelectImage;
    private EditText titleField;
    private EditText descField;
    private TextView openGallery;
    private Uri imageUri = null;
    private Button createPostButton;
    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_gallery);

        titleField = (EditText) findViewById(R.id.textTitle);
        descField = (EditText) findViewById(R.id.textDescription);
        createPostButton = (Button) findViewById(R.id.buttonUpload);
        mSelectImage = (ImageButton) findViewById(R.id.imageButton);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("GalleryUploads");
        mStorage = FirebaseStorage.getInstance().getReference();

        openGallery = (TextView)findViewById(R.id.textViewShow);

        progressBar = new ProgressDialog(this);

        mSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);
            }
        });

        createPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPosting();
            }
        });

        openGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UpdateGalleryActivity.this, GalleryActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            mSelectImage.setImageURI(imageUri);
        }
    }


    private void startPosting() {
        progressBar.setMessage("Posting...");
        progressBar.show();
        final String titleValue = titleField.getText().toString().trim();
        final String descValue = descField.getText().toString().trim();
        if(!TextUtils.isEmpty(titleValue) && !TextUtils.isEmpty(descValue) && imageUri != null){
            StorageReference filePath = mStorage.child("GalleryUploads").child(imageUri.getLastPathSegment());

            // Setup Time
            Calendar calendar = Calendar.getInstance();
            final String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

            filePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    DatabaseReference newPost = mDatabase.push();

                    newPost.child("title").setValue(titleValue);
                    newPost.child("desc").setValue(descValue);
                    newPost.child("image").setValue(downloadUrl.toString());
                    newPost.child("time").setValue(currentDate);

                    progressBar.dismiss();

                    //startActivity(new Intent(UpdateGalleryActivity.this, GalleryActivity.class));
                }
            });

        }
        else{
            progressBar.dismiss();
            Toast.makeText(UpdateGalleryActivity.this, "Please Fill up all the fields", Toast.LENGTH_SHORT).show();
        }
    }
}
