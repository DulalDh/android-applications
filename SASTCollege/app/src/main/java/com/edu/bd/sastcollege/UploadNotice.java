package com.edu.bd.sastcollege;

/**
 * Created by Animash on 6/3/2018.
 */

public class UploadNotice {
    public String name;
    public String url;
    public String time;
    public UploadNotice(){

    }
    public UploadNotice(String name, String url, String time) {
        this.name = name;
        this.url = url;
        this.time = time;
    }


    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getTime() {
        return time;
    }
}
