package com.edu.bd.sastcollege;

/**
 * Created by Animash on 6/3/2018.
 */

public class UpdateGallery {
    private String title;
    private String desc;
    private String image;
    private String time;

    public UpdateGallery(){

    }
    public UpdateGallery(String title, String desc, String image, String time) {
        this.title = title;
        this.desc = desc;
        this.image = image;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
