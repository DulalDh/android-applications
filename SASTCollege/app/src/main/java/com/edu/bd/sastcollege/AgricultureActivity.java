package com.edu.bd.sastcollege;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class AgricultureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agriculture);
    }

    public void call1(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01722560480"));
        startActivity(intent);
    }

    public void call2(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01717527326"));
        startActivity(intent);
    }

    public void call3(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01718456905"));
        startActivity(intent);
    }

    public void call4(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01776967501"));
        startActivity(intent);
    }

    public void call5(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01706737063"));
        startActivity(intent);
    }

    public void call6(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01761251330"));
        startActivity(intent);
    }

    public void call7(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01723857305"));
        startActivity(intent);
    }

    public void call8(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01737794884"));
        startActivity(intent);
    }

    public void call9(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01719347084"));
        startActivity(intent);
    }
}
