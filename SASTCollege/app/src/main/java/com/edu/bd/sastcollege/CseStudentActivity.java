package com.edu.bd.sastcollege;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class CseStudentActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    //cse 2014
    String[] c_name = {"Name: Animash Roy\nStudent ID: 1420101\nContact No: 01763235521",
            "Name: Bishwajit Roy\nStudent ID: 1420102\nContact No: 01767318962",
            "Name: Md.Shobuz Islam\nStudent ID: 1420103\nContact No: 01746961646",
            "Name: Md.Dulal Hossin\nStudent ID: 1420104\nContact No: 01751341563",
            "Name: Mst.kashmery Aktar\nStudent ID: 1420109\nContact No: 01783220343",
            "Name: Md.abdulla al Mamun\nStudent ID: 1420110\nContact No: 01774098702",
            "Name: Mst.Parul Aktar\nStudent ID: 1420111\nContact No: 01773535184",
            "Name: Shorkar Md.Saiduzzam (Sojeeb)\nStudent ID: 1420114\nContact No: 01774372537",
            "Name: Hemanta Chandra Sharma Dip\nStudent ID: 1420115\nContact No: 01751387831",
            "Name: Md.Shadequl Islam\nStudent ID: 1420118\nContact No: 01744325635",
            "Name: Md.Sohel Rana\nStudent ID: 1420119\nContact No: 01739264588",
            "Name: Md.Ismail Hossin\nStudent ID: 1420120\nContact No: 01721825387",
            "Name: Mst.Ayvi Aktar\nStudent ID: 1420123\nContact No: 01780358104",
            "Name: Md.Shariful Islam\nStudent ID: 1420124\nContact No: 01738145618",
            "Name: Mst.Monjila Khatun\nStudent ID; 1420125\nContact No: 01783233684"
//cse 2017
            ,"Name: Md Shoheb al Sojol Dewani\nStudent ID: 1620120\nContact No: 01738464828"
            ,"Name: SM Neamul Ferdus\nStudent ID: 1720101\nContact No: 01798903551"
            ,"Name: Most Rozina\nStudent ID: 1720102\nContact No: "
            ,"Name: Tauhid Hasan\nStudent ID: 1720103\nContact No: 01795267674 "
            ,"Name: Md Ratan Sarker\nStudent ID: 1720105\nContact No: 01780868145"
            ,"Name: Md Abu Hasan\nStudent ID: 1720108\nContact No: 01780866862"
            ,"Name: Kazol Roy\nStudent ID: 1720110\nContact No: 01717071664"
            ,"Name: Subas Chandra Roy\nStudent ID: 1720111\nContact No: 01752082970"
            ,"Name: Badhan Chandra Barman\nStudent ID: 1720112\nContact No: 01773848167"
            ,"Name: Md Saroar Shahi Sumon\nStudent ID: 1720113\nContact No: 01737889392"
            ,"Name: Md Tarek Aziz\nStudent ID: 1720114\nContact No: 01785285803"
            ,"Name: Md Abdul Hamid\nStudent ID: 1720116\nContact No: 01770903413"
            ,"Name: Md Touhidul Islam\nStudent ID: 1720117\nContact No: 01744251375"
            ,"Name: Tasfia Tasnim\nStudent ID: 1720118\nContact No:"
            ,"Name: Md Jafar Iqbl\nStudent ID: 1720119\nContact No: 01792813544"
            ,"Name: Sree Chandan Paul\nStudent ID: 1720120\nContact No: 01729737534"
            ,"Name: Md Kausar Ali\nStudent ID: 1720121\nContact No: 01755353827"
            ,"Name: Md Mahabul Hasan Satu\nStudent ID: 1720122\nContact No: 01783256847"
            ,"Name: Md Sohanur Rahman \nStudent ID: 1720123\nContact No: 01744503744"
            ,"Name: Md Mursalin Babu \nStudent ID: 1720124\nContact No: 01765934954"
            ,"Name: Dipangkar Chandra Roy\nStudent ID: 1720125\nContact No: 01785221743"
            ,"Name: Md Raihanur Islam\nStudent ID: 1720126\nContact No: 01738688684"
            ,"Name: Md Pabel Islam\nStudent ID: 1720127\nContact No: 01798159685"
            ,"Name: Md Himel Rana\nStudent ID: 1720128\nContact No: 01775330730"
            ,"Name: Md Al Imran Ali\nStudent ID: 1720129\nContact No: 01770903413"
            ,"Name: Md Asadujjaman\nStudent ID: 1720130\nContact No: 01736045590"
            ,"Name: Md Adnan julfikar\nStudent ID: 1720131\nContact No: 01737270837"
            ,"Name: Bristi Devnath\nStudent ID: 1720132\nContact No:"
            ,"Name: Md Abdul Lotif Jone\nStudent ID: 1720134\nContact No 01750335920",
           /* cse 2015*/
            "Name:Arnab Kumar Mndal\nStudent id:1520101\nContact No:01765570226",
            "Name:Md.Istiaque Alam Imon\nStudent id:1520102\nContact No:01796720828",
            "Name:Md.Rajiur Rahman\nStudent id:1520103\nContact No:01716009308",
            "Name:Bipul Chondro Roy\nStudent id:1520104\nContact No:01722164436",
            "Name:Md.Rabiual Aual\nStudent id:1520105\nContact No:01989856692",
            "Name:Throyee Kundu\nStudent id:1520106\nContact No:01703799933",
            "Name:Md.Al-Mamun\nStudent id:1520107\nContact No:01762708331",
            "Name:Mst.Humayra Munaim\nStudent id:1520108\nContact No:01774101487",
            "Name:Md.Ruknuzzaman Razib\nStudent id:1520109\nContact No:01767392602",
            "Name:Md.Nazmul Hasan\nStudent id:1520111\nContact No:01783288120",
            "Name:Pritilota Roy\nStudent id:1520112\nSession:2015",
            "Name:Md.Abdur Rajjak\nStudent id:1520113\nContact No:01771805156",
            "Name:Mst:Jesmin Akter\nStudent id:1520114\nSession:2015",
            "Name:Md.Ahasan Habib\nStudent id:1520115\nContact No:01521468902",
            "Name:Mst:Shima Akter\nStudent id:1520116\nSession:2015",
            "Name:Md.Mahfuzur Rahman\nStudent id:1520117\nContact No:01773832897",
            "Name:Md.Saidul Mustakim Shamim\nStudent id:1520118\nContact No:01767393646",
            "Name:Suman Mahant\nStudent id:1520119\nContact No:01723614424",
            "Name:Md.Abu Sufian\nStudent id:1520120\nContact No:01754647022",
            "Name:Md.Abdus Salam\nStudent id:1520121\nContact No:01750246648",
            "Name:Md.Golam Faruk\nStudent id:1520122\nContact No:01783003967",
            "Name:Md.Tahidur Rahman\nStudent id:1520123\nContact No:01829614171",
            "Name:Md.Sarwar Hossain\nStudent id:1520124\nContact No:01792912771",
            "Name:Md.Chaity Sarker\nStudent id:1520125\nSession:2015",
            "Name:Md.Proshanto Kumar Razak\nStudent id:1520126\nContact No:01726496811",
            "Name:Md.Alif Afnan\nStudent id:1520127\nContact No:01768498157",
            "Name:Mst:Shahina Easmin\nStudent id:1520128\nSession:2015",
            "Name:Md.Mahamudur Rahman\nStudent id:1520129\nContact No:01773376222",
            "Name:Md.Arif Hosin\nStudent id:1520130\nContact No:01784076752",
            "Name:Md.Lutfor Rahman liton\nStudent id:1520131\nContact No:01197123732",
            "Name:Md.Abu Talhad palash\nStudent id:1520132\nContact No:01723857815",
            "Name:Md.Munjur Alam\nStudent id:1520133E\nContact No:01755467748",
            "Name:Md.Arifuzzaman\nStudent id:1520134\nContact No:01750349072",
            "Name:Md.Moniruzzaman\nStudent id:1520135\nContact No:01728316365"
            };

    Button button;

    int[] c_pic = {R.drawable.ani, R.drawable.jit, R.drawable.male,R.drawable.dulal,R.drawable.male,R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male, R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male, R.drawable.male,R.drawable.male,
                   R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male,R.drawable.male, R.drawable.male,R.drawable.male,
                   R.drawable.male};


            Toolbar toolbar;
    RecyclerView recyclerView;
    RecyclerAdepter adepter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Cse> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cse_student);
        button = (Button) findViewById(R.id.call1);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        int count = 0;
        for (String name : c_name){
            arrayList.add(new Cse(name,c_pic[count]));

            count++;
        }

        adepter = new RecyclerAdepter(arrayList);
        recyclerView.setAdapter(adepter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items,menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<Cse> newList = new ArrayList<>();
        for (Cse cse : arrayList){
            String name = cse.getName().toLowerCase();
            if (name.contains(newText))
                newList.add(cse);
        }
        adepter.setFilter(newList);
        return false;
    }
    public void call1(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01751341563"));
        startActivity(intent);
    }
}
