package com.edu.bd.sastcollege;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class BbaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bba);
    }

    public void call1(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01716433488"));
        startActivity(intent);
    }

    public void call2(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01722703176"));
        startActivity(intent);
    }

    public void call3(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01724011043"));
        startActivity(intent);
    }

    public void call4(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01723784325"));
        startActivity(intent);
    }

    public void call5(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01751393846"));
        startActivity(intent);
    }

    public void call6(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01729612515"));
        startActivity(intent);
    }

    public void call7(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01773265899"));
        startActivity(intent);
    }

    public void call8(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01796720245"));
        startActivity(intent);
    }

    public void call9(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01714625588"));
        startActivity(intent);
    }

    public void call10(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01737270905"));
        startActivity(intent);
    }

    public void call11(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "017i7714968"));
        startActivity(intent);
    }

    public void call12(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01710044757"));
        startActivity(intent);
    }
}
