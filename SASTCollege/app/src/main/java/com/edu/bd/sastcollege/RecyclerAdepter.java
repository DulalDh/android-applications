package com.edu.bd.sastcollege;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class RecyclerAdepter extends RecyclerView.Adapter<RecyclerAdepter.MyViewHolder>{

    ArrayList<Cse> arrayList = new ArrayList<>();

    RecyclerAdepter(ArrayList<Cse> arrayList){
        this.arrayList = arrayList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.c_pic.setImageResource(arrayList.get(position).getId_name());
        holder.c_name.setText(arrayList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView c_pic;
        TextView c_name;


        public MyViewHolder(View itemView) {
            super(itemView);
            c_pic = (ImageView) itemView.findViewById(R.id.image);
            c_name = (TextView) itemView.findViewById(R.id.info);

        }
    }

    public void setFilter (ArrayList<Cse> newList){
        arrayList = new ArrayList<>();
        arrayList.addAll(newList);
        notifyDataSetChanged();

    }
}

