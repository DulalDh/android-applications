package com.edu.bd.sastcollege;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class CseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cse);
    }


    public void call1(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01736410924"));
        startActivity(intent);
    }

    public void call2(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01776824639"));
        startActivity(intent);
    }

    public void call3(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01723209854"));
        startActivity(intent);
    }

    public void call4(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01722562159"));
        startActivity(intent);
    }

    public void call5(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01721569230"));
        startActivity(intent);
    }

    public void call6(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01738541844"));
        startActivity(intent);
    }

    public void call7(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01792792244"));
        startActivity(intent);
    }

    public void call8(View view){
        Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01719512458"));
        startActivity(intent);
    }
}
