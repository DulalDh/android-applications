package com.edu.bd.sastcollege;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CollegeActivity extends AppCompatActivity {

    Button button2;
    Button button3;
    Button button4;
    Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_college);
        button2 = (Button)findViewById(R.id.button2);
        button4 = (Button)findViewById(R.id.button4);
        button3 = (Button)findViewById(R.id.button3);
        button1 = (Button)findViewById(R.id.button1);


        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CollegeActivity.this, TeacherActivity.class);
                startActivity(intent);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CollegeActivity.this, AdmissionActivity.class);
                startActivity(intent);
            }
        });


        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CollegeActivity.this, EmployeActivity.class);
                startActivity(intent);
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CollegeActivity.this, StudentActivity.class);
                startActivity(intent);
            }
        });
    }
}
